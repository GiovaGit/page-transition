import Link from "next/link"

export default function Home() {
  return (
    <div className='flex items-center justify-center w-screen h-screen bg-gradient-to-b from-orange-400 to-orange-500'>
      <Link href='/destination'>
        <a className='text-[5vw] leading-none'>
          🍑🍑🍑
          <br />
          🍑🍑🍑
          <br />
          🍑🍑🍑
        </a>
      </Link>
    </div>
  )
}
