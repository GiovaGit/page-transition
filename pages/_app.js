import { PageTransitionPanels } from "../components/transition_panel"
import "../styles/globals.css"
import "../styles/text.css"

function MyApp({ Component, pageProps }) {
  const withTranslateConfig = {
    isRotate: false,
    isInverted: false,
    isRewind: false,
    delay: 0.2,
    duration: 0.8,
    entryEase: "easeIn",
    exitEase: "easeOut",
  }

  const withRewindTranslateConfig = {
    isRotate: false,
    isInverted: false,
    isRewind: true,
    delay: 0.2,
    duration: 0.8,
    entryEase: "easeIn",
    exitEase: "easeOut",
  }

  const withRotateConfig = {
    isRotate: true,
    isInverted: true,
    isRewind: false,
    delay: 0.2,
    duration: 0.8,
    entryEase: "easeInOut",
    exitEase: "easeInOut",
  }

  const withRewindRotateConfig = {
    isRotate: true,
    isInverted: true,
    isRewind: true,
    delay: 0.2,
    duration: 0.8,
    entryEase: "easeInOut",
    exitEase: "easeInOut",
  }

  return (
    <>
      <PageTransitionPanels
        {...withTranslateConfig}
        // {...withRewindTranslateConfig}
        // {...withRotateConfig}
        // {...withRewindRotateConfig}
        panels={[
          { leftColor: "#023e8a", rightColor: "#023e8a" },
          { leftColor: "#0096c7", rightColor: "#0096c7" },
          { leftColor: "#48cae4", rightColor: "#48cae4" },
          { leftColor: "#ade8f4", rightColor: "#ade8f4" },
        ]}
      />
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
