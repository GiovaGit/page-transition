import Router from "next/router"
import { useEffect, useState } from "react"
import { AnimatePresence } from "framer-motion"
import { TranslatePanel } from "./translate_panel"
import { RotatePanel } from "./rotate_panel"

export const PageTransitionPanels = ({
  isRotate = false,
  isInverted = false,
  isRewind = false,
  delay = 0.5,
  duration = 1,
  entryEase = "easeIn",
  exitEase = "easeOut",
  panels = [{ leftColor: "red", rightColor: "blue" }],
}) => {
  const [isPanelVisible, setIsPanelVisible] = useState(false)
  const showPanel = () => setIsPanelVisible(true)
  const hidePanel = () => setTimeout(() => setIsPanelVisible(false), 500)

  useEffect(() => {
    Router.events.on("routeChangeStart", showPanel)
    return () => Router.events.off("routeChangeStart", showPanel)
  }, [])

  return (
    <AnimatePresence>
      {isPanelVisible &&
        panels.map((panel, i) => {
          const entryDelay = i * delay
          const exitDelay = (panels.length - 1 - i) * delay

          const isLastPanel = i === panels.length - 1
          const isPanelInverted = isInverted && i % 2 === 0

          return isRotate ? (
            <RotatePanel
              key={`panel_${i}`}
              isInverted={isPanelInverted}
              isRewind={isRewind}
              duration={duration}
              entryDelay={entryDelay}
              exitDelay={exitDelay}
              entryEase={entryEase}
              exitEase={exitEase}
              leftColor={panel.leftColor}
              rightColor={panel.rightColor}
              onAnimationComplete={isLastPanel && hidePanel}
            />
          ) : (
            <TranslatePanel
              key={`panel_${i}`}
              isRewind={isRewind}
              duration={duration}
              entryDelay={entryDelay}
              exitDelay={exitDelay}
              entryEase={entryEase}
              exitEase={exitEase}
              leftColor={panel.leftColor}
              rightColor={panel.rightColor}
              onAnimationComplete={isLastPanel && hidePanel}
            />
          )
        })}
    </AnimatePresence>
  )
}
