import { motion } from "framer-motion"

export const TranslatePanel = ({
  isRewind,
  duration,
  entryDelay,
  exitDelay,
  entryEase,
  exitEase,
  leftColor,
  rightColor,
  onAnimationComplete = () => console.log("no callback specified"),
}) => {
  const visibleTransition = { duration: duration, delay: entryDelay, ease: entryEase }
  const exitTransition = { duration: duration, delay: exitDelay, ease: exitEase }

  const variants = {
    hidden: { x: "100vw" },
    visible: { x: 0, transition: visibleTransition },
    exit: { x: isRewind ? "100vw" : "-100vw", transition: exitTransition },
  }

  return (
    <motion.div
      variants={variants}
      initial='hidden'
      animate='visible'
      exit='exit'
      onAnimationComplete={onAnimationComplete}
      className='absolute w-screen h-screen z-10'
      style={{
        background: `linear-gradient(to right, ${leftColor}, ${rightColor})`,
      }}
    />
  )
}
