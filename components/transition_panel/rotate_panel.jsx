import { motion } from "framer-motion"

export const RotatePanel = ({
  isInverted,
  isRewind,
  duration,
  entryDelay,
  exitDelay,
  entryEase,
  exitEase,
  leftColor,
  rightColor,
  onAnimationComplete = () => console.log("no callback specified"),
}) => {
  const visibleTransition = { duration: duration, delay: entryDelay, ease: entryEase }
  const exitTransition = { duration: duration, delay: exitDelay, ease: exitEase }

  const variants = {
    hidden: { rotate: 0, originX: isInverted ? 1 : 0, originY: 1 },
    visible: { rotate: isInverted ? 90 : -90, transition: visibleTransition },
    exit: { rotate: isRewind ? 0 : isInverted ? 180 : -180, transition: exitTransition },
  }

  return (
    <motion.div
      variants={variants}
      initial='hidden'
      animate='visible'
      exit='exit'
      onAnimationComplete={onAnimationComplete}
      className='absolute w-[max(200vh,200vw)] h-[max(200vh,200vw)] z-10 bottom-0'
      style={{
        x: isInverted ? "min(-200vh,-200vw)" : "100vw",
        background: `linear-gradient(to right, ${leftColor}, ${rightColor})`,
      }}
    />
  )
}
